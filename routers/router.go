////Version: v0.0.1
package routers

import (
	"git.forms.io/isaving/sv/ssv1000016/controllers"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-12
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.String(constant.TopicPrefix+"ssv1000016"),
		&controllers.Ssv1000016Controller{}, "Ssv1000016")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-12
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv1000016",
		beego.NSNamespace("/ssv1000016",
			beego.NSInclude(
				&controllers.Ssv1000016Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
