//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000016/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000016Controller struct {
	controllers.CommTCCController
}

func (*Ssv1000016Controller) ControllerName() string {
	return "Ssv1000016Controller"
}

// @Desc ssv1000016 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000016Controller) Ssv1000016() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000016Controller.Ssv1000016 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000016I := &models.SSV1000016I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000016I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000016I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000016 := &services.Ssv1000016Impl{}
	ssv1000016.New(c.CommTCCController)
	ssv1000016.Ssv100016I = ssv1000016I
	ssv1000014Compensable := services.Ssv1000016Compensable

	proxy, err := aspect.NewDTSProxy(ssv1000016, ssv1000014Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000016I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000016O, ok := rsp.(*models.SSV1000016O); ok {
		if responseBody, err := models.PackResponse(ssv1000016O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv1000016 Controller
// @Description ssv1000016 controller
// @Param Ssv1000016 body model.SSV1000016I true body for SSV1000016 content
// @Success 200 {object} model.SSV1000016O
// @router /create [post]
func (c *Ssv1000016Controller) SWSsv1000016() {
	//Here is to generate API documentation, no need to implement methods
}
