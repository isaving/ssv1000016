//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package event

type GlobalTxnResultReport struct {
	Head    TxnEventHeader            `json:"head"`
	Request GlobalTxnResultReportBody `json:"request"`
}

type GlobalTxnResultReportBody struct {
	//Org       string `json:"org"`
	//Az        string `json:"az"`
	//Dcn       string `json:"dcn"`
	//ServiceId string `json:"serviceId"`
	//NodeId    string `json:"nodeId"`
	RootXid string `json:"rootXid"`
	// 1-全部confirm成功
	// 2-全部cancel成功
	GTxnStat    string    `json:"gStat"`
	GTxnIsDone  bool      `json:"gIsDone"`
	GTxnStartTm string    `json:"gStartTm"`
	GTxnEndTm   string    `json:"gEndTm"`
	TxnList     []TxnInfo `json:"txnList"`
}

type TxnInfo struct {
	//Org        string `json:"org"`
	//Az         string `json:"az"`
	//Dcn        string `json:"dcn"`
	//ServiceId  string `json:"serviceId"`
	//NodeId     string `json:"nodeId"`
	//InstanceId string `json:"instanceId"`
	ParentXid    string `json:"pXid"`
	BranchXid    string `json:"bXid"`
	TxnIsDone    bool   `json:"isDone"`
	ErrorMessage string `json:"errorMessage"`
	TxnStartTm   string `json:"startTm"`
	TxnEndTm     string `json:"endTm"`
}
