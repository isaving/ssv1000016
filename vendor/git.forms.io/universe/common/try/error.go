//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package try

import (
	"fmt"
)

const (
	ERROR_CODE_LEN          = 6
	UNIVERSE_SUCC_CODE      = 0      /* Succsesful Code */
	UNIVERSE_DEFAULT_CODE   = 999999 /* Default Code */
	UNIVERSE_MODULE_DEFAULT = 10     /* Default */
	UNIVERSE_MODULE_DLS     = 66     /* DLS MODULE */
)

type Exception struct {
	Code    uint
	Message string
	Data    *interface{}
}

func (e *Exception) Error() string {
	return fmt.Sprintf("[%d] %s", e.Code, e.Message)
}

func Successful(data *interface{}) *Exception {
	return &Exception{
		Code:    UNIVERSE_SUCC_CODE,
		Message: "O.K.",
		Data:    data,
	}
}

func Throwble(err *error) *Exception {
	return Error(UNIVERSE_DEFAULT_CODE, *err)
}

func Errorf(code int, f string, args ...interface{}) *Exception {
	return &Exception{
		Code:    uint(code),
		Message: fmt.Sprintf(f, args...),
		Data:    nil,
	}
}

func Append(code int, err error, f string, args ...interface{}) *Exception {
	return &Exception{
		Code:    uint(code),
		Message: fmt.Sprintf("%s %s", fmt.Sprintf(f, args...), err.Error()),
		Data:    nil,
	}
}

func Error(c int, e error) *Exception {
	if e == nil {
		return nil
	}
	switch e.(type) {
	case *Exception:
		return e.(*Exception)
	default:
		return &Exception{
			Code:    uint(c),
			Message: e.Error(),
			Data:    nil,
		}
	}
}
