package util

import (
	"fmt"
	"git.forms.io/legobank/legoapp/config"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/universe/comm-agent/client"
	common "git.forms.io/universe/comm-agent/common/protocol"
	"git.forms.io/universe/solapp-sdk/util"
	"github.com/astaxie/beego"
	"sync"
	"time"
)

var seqNo int64 = 0
var lock sync.Mutex

const SEQMAX = 999999

func InitHearderMap(request *client.UserMessage, srcSysId, key string) (*map[string]string, *map[string]string) {

	var hearderMap = make(map[string]string)
	var newSrcMap = make(map[string]string)

	hearderMap[constant.SRCSYSID] = request.AppProps[constant.SRCSYSID]
	hearderMap[constant.SRCDCN] = request.AppProps[constant.SRCDCN]
	hearderMap[constant.SRCSERVICEID] = request.AppProps[constant.SRCSERVICEID]
	hearderMap[constant.SRCTIMESTAMP] = request.AppProps[constant.SRCTIMESTAMP]
	hearderMap[constant.SRCTOPOICID] = request.AppProps[constant.SRCTOPOICID]
	hearderMap[constant.SRCBIZSEQNO] = request.AppProps[constant.SRCBIZSEQNO]
	hearderMap[constant.SRCBIZDATE] = request.AppProps[constant.SRCBIZDATE]
	hearderMap[constant.TRGSERVICEID] = config.ServiceConf.AppName
	hearderMap[constant.TRGSERVICEVERSION] = ""
	hearderMap[constant.TRGTOPOICID] = beego.AppConfig.String("topic::" + key)
	hearderMap[constant.TRGBIZDATE] = time.Now().Local().Format("20060102")
	hearderMap[constant.TRGTIMESTAMP] = time.Now().Local().Format("2006-01-02 15:04:05.000")

	hearderMap[constant.PARENT_SPAN_ID] = request.AppProps[constant.PARENT_SPAN_ID]

	newSrcMap[constant.SRCSYSID] = srcSysId
	newSrcMap[constant.SRCDCN] = config.ServiceConf.DataCenterNode
	newSrcMap[constant.SRCSERVICEID] = config.ServiceConf.AppName
	newSrcMap[constant.SRCTIMESTAMP] = time.Now().Local().Format("2006-01-02 15:04:05.000")
	newSrcMap[constant.SRCTOPOICID] = beego.AppConfig.String("topic::" + key)
	newSrcMap[constant.SRCBIZDATE] = time.Now().Local().Format("20060102")
	newSrcMap[constant.TRGSERVICEID] = ""
	newSrcMap[constant.TRGSERVICEVERSION] = ""
	newSrcMap[constant.TRGTOPOICID] = ""
	newSrcMap[constant.TRGBIZDATE] = ""
	newSrcMap[constant.TRGBIZSEQNO] = ""
	newSrcMap[constant.TRGTIMESTAMP] = ""

	if bizSeqNo, ok := request.AppProps[constant.SRCBIZSEQNO]; ok && bizSeqNo != "" {
		newSrcMap[constant.PARENT_SPAN_ID] = request.AppProps[constant.SRCBIZSEQNO]
	} else {
		newSrcMap[constant.PARENT_SPAN_ID] = request.AppProps[constant.GLOBALBIZSEQNO]
	}

	if _, existed := request.AppProps[common.DLS_ELEMENT_TYPE]; existed {
		request.AppProps[common.DLS_ELEMENT_TYPE] = ""
	}
	if _, existed := request.AppProps[common.DLS_ELEMENT_ID]; existed {
		request.AppProps[common.DLS_ELEMENT_ID] = ""
	}
	if _, existed := request.AppProps[common.TARGET_DCN]; existed {
		request.AppProps[common.TARGET_DCN] = ""
	}

	return &hearderMap, &newSrcMap
}

func getSerialNo(serviceType string) string {

	dcnNo := config.ServiceConf.DataCenterNode
	instanceID := config.ServiceConf.InstanceID
	timeStamp := time.Now().Unix()

	lock.Lock()
	defer lock.Unlock()
	seqNo++
	if seqNo > SEQMAX {
		seqNo = 1
	}

	return fmt.Sprintf("%s%s%s%d%06d", serviceType, dcnNo, instanceID, timeStamp, seqNo)
}

func getBusinessSerialNo() string {
	return util.GenerateSerialNo("1")
}

func GetGlobalSerialNo() string {
	return util.GenerateSerialNo("0")
}

func setHeader(request *client.UserMessage, topMap map[string]string) {
	for key, value := range topMap {
		request.AppProps[key] = value
	}
}
