//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000016/constant"
	constant2 "git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv1000016Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000016",
	ConfirmMethod: "ConfirmSsv1000016",
	CancelMethod:  "CancelSsv1000016",
}

type Ssv1000016 interface {
	TrySsv1000016(*models.SSV1000016I) (*models.SSV1000016O, error)
	ConfirmSsv1000016(*models.SSV1000016I) (*models.SSV1000016O, error)
	CancelSsv1000016(*models.SSV1000016I) (*models.SSV1000016O, error)
}

type Ssv1000016Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv100016O  *models.SSV1000016O
	Ssv100016I  *models.SSV1000016I
	Ac000004O   *models.SAC0000004O
	srcBizSeqNo string
}

// @Desc Ssv1000016 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000016Impl) TrySsv1000016(ssv1000016I *models.SSV1000016I) (ssv1000016O *models.SSV1000016O, err error) {

	impl.Ssv100016I = ssv1000016I
	impl.srcBizSeqNo = impl.SrcAppProps[constant2.SRCBIZSEQNO]

	//调用存款扣款BSS
	err = impl.AccountDeduction()
	if err != nil {
		return nil, err
	}

	ssv1000016O = &models.SSV1000016O{
		AmtFreezing:  impl.Ac000004O.AmtFreezing,  //冻结金额
		AmtCurrent:   impl.Ac000004O.AmtCurrent,   //当前余额
		AmtLast:      impl.Ac000004O.AmtLast,      //上期余额
		AmtAvaliable: impl.Ac000004O.AmtAvailable, //可用余额
		AccStatus:    impl.Ac000004O.AccStatus,    //账户状态
	}

	return ssv1000016O, nil
}

//存款扣款BSS
func (impl *Ssv1000016Impl) AccountDeduction() error {
	sac0000004I := models.SAC0000004I{
		CustId:        impl.Ssv100016I.CustId,     //客户号
		ContractId:    impl.Ssv100016I.ContractId, //合约号
		Account:       impl.Ssv100016I.Account,    //核算账号
		Amount:        impl.Ssv100016I.Amount,     //交易金额
		TrnDate:       impl.Ssv100016I.TrnDate,    //交易日期
		TrnSeq:        impl.srcBizSeqNo,           //交易流水号
		FlowSeq:       1,
		DeductionFlag: impl.Ssv100016I.DeductionFlag,
	}

	//pack request
	reqBody, err := sac0000004I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.AC000004, reqBody)
	if err != nil {
		return err
	}

	ac000004O := models.SAC0000004O{}
	err = ac000004O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	impl.Ac000004O = &ac000004O

	return nil
}

func (impl *Ssv1000016Impl) ConfirmSsv1000016(ssv1000016I *models.SSV1000016I) (ssv1000016O *models.SSV1000016O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv1000016")
	return nil, nil
}

func (impl *Ssv1000016Impl) CancelSsv1000016(ssv1000016I *models.SSV1000016I) (ssv1000016O *models.SSV1000016O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv1000016")
	return nil, nil
}
