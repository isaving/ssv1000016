//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var AC000004 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "AmtFreezing":1,
		"AmtCurrent":1,
		"AmtLast":1,
		"AmtAvaliable":1,
		"AccStatus":"1"
    }
}`

func (this *Ssv1000016Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "AC000004":
		responseData = []byte(AC000004)

	}

	return responseData, nil
}

func TestSsv1000016Impl_Ssv1000016(t *testing.T) {

	Ssv10000161Impl := new(Ssv1000016Impl)

	_, _ = Ssv10000161Impl.TrySsv1000016(&models.SSV1000016I{
		CustId:     "1",
		ContractId: "1",
		Account:    "1",
		Amount:     1,
		TrnDate:    "1",
		TrnSeq:     "1",
		FlowSeq:    1,
	})

	_, _ = Ssv10000161Impl.CancelSsv1000016(&models.SSV1000016I{})
	_, _ = Ssv10000161Impl.ConfirmSsv1000016(&models.SSV1000016I{})

}
